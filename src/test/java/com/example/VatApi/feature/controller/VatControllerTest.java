package com.example.VatApi.feature.controller;

import com.example.VatApi.feature.dto.internal.WhitelistRequestDto;
import com.example.VatApi.feature.serivce.VatService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VatControllerTest {

    @Mock
    VatService vatService;
    @InjectMocks
    VatController vatController;

    @Test
    void isBankAccountValid() {
        //given
        WhitelistRequestDto whitelistRequestDto = WhitelistRequestDto.builder()
                .bankAccountNumbers(List.of("43249000050000453053451934", "34124012391111001093253304"))
                .build();
        Map<String, Boolean> mockAccountValidationResponse = Map.of("43249000050000453053451934", true, "34124012391111001093253304", true);

        when(vatService.validateBankAccounts(whitelistRequestDto)).thenReturn(mockAccountValidationResponse);

        //when
        Map<String, Boolean> mockResult = vatController.isBankAccountValid(whitelistRequestDto);

        //then
        assertThat(mockResult).hasSize(2);
        verify(vatService, times(1)).validateBankAccounts(whitelistRequestDto);
    }
}
