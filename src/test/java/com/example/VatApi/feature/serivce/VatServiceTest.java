package com.example.VatApi.feature.serivce;

import com.example.VatApi.feature.dto.external.EntryDto;
import com.example.VatApi.feature.dto.external.SubjectDto;
import com.example.VatApi.feature.dto.external.WhiteListResult;
import com.example.VatApi.feature.dto.external.WhitelistResponseDto;
import com.example.VatApi.feature.dto.internal.WhitelistRequestDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VatServiceTest {

    VatService vatService;
    @Mock
    RestTemplate restTemplate;

    String mockUrl = "mock.url";

    static final String ACCOUNT_ON_WHITE_LIST = "43249000050000453053451934";
    static final String ACCOUNT_NOT_ON_WHITE_LIST = "83114020040000390270870249";
    @BeforeEach
    public void initMocks() {
        vatService = new VatService(restTemplate, mockUrl);
    }

    @Test
    void validateBankAccounts() {
        //given
        WhitelistRequestDto whitelistRequestDto = WhitelistRequestDto.builder()
                .bankAccountNumbers(List.of(ACCOUNT_ON_WHITE_LIST, ACCOUNT_NOT_ON_WHITE_LIST))
                .build();
        WhitelistResponseDto mockResponse = creatMockResponseDto(List.of(ACCOUNT_ON_WHITE_LIST));

        when(restTemplate.getForObject(anyString(), eq(WhitelistResponseDto.class))).thenReturn(mockResponse);
        //when

        Map<String, Boolean> result = vatService.validateBankAccounts(whitelistRequestDto);
        //then
        verify(restTemplate, times(1)).getForObject(anyString(), eq(WhitelistResponseDto.class));
        assertThat(result).hasSize(2);
        assertThat(result.get(ACCOUNT_ON_WHITE_LIST)).isTrue();
        assertThat(result.get(ACCOUNT_NOT_ON_WHITE_LIST)).isFalse();
        }

    private WhitelistResponseDto creatMockResponseDto(final List<String> bankAccountNumbers) {
        SubjectDto subjectDto = SubjectDto.builder()
                .accountNumbers(bankAccountNumbers)
                .build();
        EntryDto entryDto = EntryDto.builder()
                .subjects(List.of(subjectDto))
                .build();
        WhiteListResult listResult = WhiteListResult.builder()
                .entries(List.of(entryDto))
                .build();
        return WhitelistResponseDto.builder()
                .result(listResult)
                .build();
    }
}
