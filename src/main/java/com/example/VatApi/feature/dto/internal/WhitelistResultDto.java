package com.example.VatApi.feature.dto.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WhitelistResultDto {
    private Map<String, Boolean> isBankAccountOnTheList;
}
