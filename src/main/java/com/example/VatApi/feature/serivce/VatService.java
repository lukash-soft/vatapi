package com.example.VatApi.feature.serivce;

import com.example.VatApi.feature.dto.external.EntryDto;
import com.example.VatApi.feature.dto.external.WhitelistResponseDto;
import com.example.VatApi.feature.dto.internal.WhitelistRequestDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VatService {

    private final RestTemplate restTemplate;
    private final String baseUrl;

    public VatService(RestTemplate restTemplate,
                      @Value("${vat.api}") final String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    public Map<String, Boolean> validateBankAccounts(WhitelistRequestDto whitelistRequestDto) {
        String bankAccountsJoined = String.join(",", whitelistRequestDto.getBankAccountNumbers());

        String url = baseUrl + bankAccountsJoined + "?date=" + LocalDate.now();
        WhitelistResponseDto response = restTemplate.getForObject(url, WhitelistResponseDto.class);

        List<EntryDto> entries = response.getResult().getEntries();
        Map<String, Boolean> requestedAccountStatus = new HashMap<>();

        List<String> whiteListAccounts = prepareWhiteListAccounts(entries);

        for (String bankAccountToBeChecked : whitelistRequestDto.getBankAccountNumbers()) {
            requestedAccountStatus.put(bankAccountToBeChecked, whiteListAccounts.contains(bankAccountToBeChecked));
        }
        return requestedAccountStatus;
    }

    private static List<String> prepareWhiteListAccounts(List<EntryDto> entries) {
        List<String> whiteListAccounts = new ArrayList<>();
        for (EntryDto entry : entries) {
            entry.getSubjects().forEach(subject -> {
                whiteListAccounts.addAll(subject.getAccountNumbers());
            });
        }
        return whiteListAccounts;
    }

}
