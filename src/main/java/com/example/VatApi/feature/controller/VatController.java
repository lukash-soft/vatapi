package com.example.VatApi.feature.controller;

import com.example.VatApi.feature.dto.internal.WhitelistRequestDto;
import com.example.VatApi.feature.serivce.VatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RequestMapping("/account/whitelist")
@RestController
@RequiredArgsConstructor
public class VatController {

    private final VatService vatService;

    @PostMapping()
    public Map<String, Boolean> isBankAccountValid(@RequestBody WhitelistRequestDto whitelistRequestDto){
        return vatService.validateBankAccounts(whitelistRequestDto);
    }

}
