package com.example.VatApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VatApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VatApiApplication.class, args);
	}

}
